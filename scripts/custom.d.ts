/// <reference path="references.d.ts" />

/* JQuery BugFix fuer Properties */
interface JQuery {
  prop(propertyName: string, value: any): JQuery;
  realWidth(): number;
  realHeight(): number;
  serializeFormJSON();
}

interface Date {
  dateAdd(size: string, value: number): Date;
}

interface String {
  startsWith(search: string, position?: number): boolean;
  endsWith(search: string, position?: number): boolean;
  contains(search: string) : boolean;
}

declare function empty(o: any): boolean;
declare function ready(callback: Function);

declare function RenderHandlebarById(templateId: string, item: Object, target?: JQuery) : string;
declare function RenderHandlebar(template: string, item: Object, target?: JQuery) : string;

interface Handlebars {
  registerHelper(name: string, funct: Function);
}

declare function ga(action: string, options: any);
declare function ga(action: string, type: string, options: any);

declare var grecaptcha: GoogleRecaptcha;

interface GoogleRecaptcha {
  getResponse(): boolean;
  render(el, options: any);
}