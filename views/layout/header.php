<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Benjamin Gschwender · Web-Freak, Softwareentwickler, (IT) Projektleiter</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->

  <?php require_once __DIR__ . '/styles.php'; ?>

  <script>
    var readyCallbacks = [];
    function ready(callback) {
      if (typeof window["bg"] !== 'undefined') {
        bg.ready(callback);
      } else {
        readyCallbacks.push(callback);
      }
    }
  </script>
</head>

<body class="">
<header>
  <div class="header-top">
    <div class="header-top-info">
      <span>Benjamin Gschwender</span>
    </div>
    <div class="header-top-nav">
      <a href="#" onclick="bg.toggleMenue(); return false;"><span class="glyphicon"></span></a>
    </div>
  </div>
</header>