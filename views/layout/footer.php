<footer>
</footer>

<nav class="nav">
  <div class="nav-body">
    <ul>
      <li><a href="#" onclick="bg.toggleMenue(); bg.scrollToElement($('body')); event.preventDefault();">Home</a></li>
      <li><a href="#" onclick="bg.toggleMenue(); bg.scrollToElement($('#section1')); event.preventDefault();">Über mich</a></li>
      <!--<li><a href="#" onclick="bg.toggleMenue(); bg.scrollToElement($('#skills')); event.preventDefault();">Skills</a></li>-->
      <li><a href="#" onclick="bg.toggleMenue(); bg.scrollToElement($('#contact')); event.preventDefault();">Kontakt</a></li>
      <li><a href="#" onclick="bg.toggleMenue(); bg.showImprint(); event.preventDefault();">Impressum / Datenschutz</a></li>
    </ul>
  </div>
    <div class="nav-footer">
        <div class="pull-left social-links hidden-xs">
            <ul>
                <!--<li><a href="https://www.facebook.com/bgschwender" target="_blank"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>-->
                <li><a href="https://twitter.com/gschwender" target="_blank"><i class="fa fa-twitter"
                                                                                aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <div class="pull-right">
            <span>&copy; 2023 Benjamin Gschwender</span>
        </div>
    </div>
</nav>

<?php require_once __DIR__ . '/scripts.php'; ?>
</body>
</html>