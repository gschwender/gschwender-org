<?php

namespace Gitware\SimplePage;

use Gitware\SimplePage\App;

require_once __DIR__ . '/utility.php';
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/app.php';
require_once __DIR__ . '/RequestOptimizations/load.php';