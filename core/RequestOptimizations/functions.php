<?php
function getDataURI($local, $url, $mime = '') {
  if(file_exists($local) && filesize($local) < 8000) {
    return 'data: ' . (function_exists('mime_content_type') ? mime_content_type($local) : $mime) . ';base64,' . base64_encode(file_get_contents($local));
  } else {
    return $url;
  }
}

function getFileObj($url) {
  if(startsWith($url, URL_SHOP)) {
    $url = substr($url, strlen(URL_SHOP));
  }
  if(!startsWith($url, "http")) {
    $path = $url;
    if (strcontains($path, "?")) {
      $path = substr($path, 0, strpos($path, "?"));
    }
    $local = PFAD_ROOT . $path;
    $o = new stdClass();
    $o->local = PFAD_ROOT . $path;
    $o->exists = file_exists($o->local);
    if($o->exists) {
      $o->size = filesize($o->local);
      $o->time = filemtime($o->local);
    }
    $o->url = $url;
    return $o;
  }
  return null;
}

function urlWithTime($url) {
  if($url && $url != "") {
    $local = PFAD_ROOT . $url;
    if (file_exists($local)) {
      $time = filemtime($local);
      return $url . "?t=" . $time;
    }
  }
  return $url;
}

function pqCss($el, $propertyName = null, $value = null) {
  foreach($el->stack(1) as $node) {
    if (is_array($propertyName) || ! is_null($value)) {
      if ( is_array($propertyName) ) {
        $map = $propertyName;
      } else {
        $map = array($propertyName => $value);
      }
      $_new_map = array();
      if ($node->hasAttribute("style")) $styleValue = $node->getAttribute("style");
      if ($styleValue != "") $_new_map = explodeStyle($styleValue);
      foreach($map as $prop => $a) {
        if ($a == "") {
          unset($_new_map[$prop]);
        } else {
          $_new_map[$prop] = $a;
        }
      }
      $node->setAttribute("style", implodeStyle($_new_map));
      //$el->attrEvents($propertyName, "style", $styleValue, $node);
    } else {
      $propValue = "";
      if ($node->hasAttribute("style"))
      {
        $styleValue = $node->getAttribute("style");
        $styleMap = explodeStyle($styleValue);
        $propValue = $styleMap[$propertyName];
      }
      return $propValue;
    }
  }
  return (is_null($value) && ! is_array($propertyName))
    ? '' : $el;
}

function implodeStyle($map) {
  $styleText = "";
  foreach ($map as $prop => $value) {
    if ($prop != "") $arrStyle[] = $prop . ":" . $value;
  }
  $styleText = implode(";", $arrStyle);
  return $styleText;
}

function explodeStyle($style) {
  $map = array();
  $arrStyle = explode(";", $style);
  foreach ($arrStyle as $value) {
    $prop = explode(":", $value);
    $map[$prop[0]] = $prop[1];
  }
  return $map;
}


?>